package com.goldina.fragmentnav

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : Fragment() {
    private lateinit var bottom_nav:BottomNavigationView
    private lateinit var rcView:RecyclerView
    private val adapter = MyStringAdapter()
    private lateinit var textView: TextView
    private var style = R.style.tv1
    private val bookIDLis = listOf("Портрет Дориана Грея"
        ,"Мальчик в полосатой пижаме"
        ,"Заповедник"
        ,"Рита Хейуорт, или Побег из Шоушенка"
        ,"Пышка"
        ,"Десять негритят")
    private val authorIDLis = listOf("Оскар Уайльд"
        ,"Джон Бойн"
        ,"Сергей Довлатов"
        ,"Стивен Кинг"
        ,"Ги де Мопассан"
        ,"Агата Кристи")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bottom_nav = activity?.findViewById(R.id.bottom_nav)!!
        bottom_nav.isVisible=true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        textView=activity?.findViewById(R.id.textViewCheck)!!
        rcView=view.findViewById(R.id.rcView)
        init()
        return view
    }

    private fun init(){
        view.apply {
            rcView.layoutManager=LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false)
            rcView.adapter=adapter
            if(textView.text!="0") style = textView.text.toString().toInt()
            for (index in 0..5) {
                    val text = "${index}. ${bookIDLis[index]} Автор: ${authorIDLis[index]}"
                    adapter.addString(MyString(text,style))
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = HomeFragment()
    }
}