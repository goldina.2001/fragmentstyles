package com.goldina.fragmentnav

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.isVisible
import com.goldina.fragmentnav.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bottomNav.isVisible=false
        binding.bottomNav.setOnItemSelectedListener{
            when(it.itemId){
                R.id.home -> supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainerView,HomeFragment())
                    .commit()
                R.id.web->{
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainerView,WebFragment())
                        .addToBackStack( "tag" ).commit();
                }
                R.id.style ->{
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fragmentContainerView,StyleFragment())
                        .addToBackStack( "tag" ).commit();
                }
            }
            true
        }
    }
}